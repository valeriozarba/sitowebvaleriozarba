const http = require('http');
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const path = require('path');

const port= '3000';

// create a new Express application instance 
const app = express();
app.use(express.static(process.cwd()+"/dist/personaleAngular/"));

const sendMail = (user, callback) => {
    const transporter = nodemailer.createTransport({
      host: "smtp.valeriozarba.it",
      port: 587,
      secure: false,
      auth: {
        user: "<info@valeriozarba.it>",
        pass: "<password>"
      }
    });

    const mailOptions = {
    from: `"<Sender’s name>", "<Sender’s email>"`,
    to: `<${user.email}>`,
    subject: "<Message subject>",
    html: "<h1>And here is the place for HTML</h1>"
    };
    transporter.sendMail(mailOptions, callback); 
}

//configure the Express middleware to accept CORS requests and parse request body into JSON
app.use(cors({origin: "*" }));
app.use(bodyParser.json());



//start application server on port 3000
app.listen(port, () => {
  console.log("The server started on port 3000");
});


  
// define a sendmail endpoint, which will send emails and response with the corresponding status
app.post("/sendmail", (req, res) => {
  console.log("creo la richiesta");
  let user = req.body;
  sendMail(user, (err, info) => {
    if (err) {
      console.log(err);
      res.status(400);
      res.send({ error: "Errore invio email" });
    } else {
      console.log("Email inviata correttamente");
      res.send(info);
    }
  });
  
});