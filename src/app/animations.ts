import { trigger, transition, style, query, animateChild, group, animate, stagger } from '@angular/animations';

export const slideInAnimation =
trigger('routeAnimations', [
  transition(':enter', [
   
      animate('0.3s ease', style({
        transform: 'translateX(50px)',
        width: 10
      })),
     
  ])
]);
