export enum SockEvent {
    CONNECT = 'connect',
    DISCONNECT = 'disconnect'
}