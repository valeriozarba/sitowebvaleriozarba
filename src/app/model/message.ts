import { Action } from './action.enum';
import { User } from './user';

export interface Message {
    from?: User;
    content?: any;
    action?: Action;
}