import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { RouterOutlet, ActivatedRoute } from '@angular/router';
import { slideInAnimation } from './animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations:[slideInAnimation]
})
export class AppComponent implements OnInit{

  toggleChanged = false;
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  
  

  title = 'Valerio Zarba / Wordpress Developer';
  stateMenu = false;
  pageActive  = 'home';
  messages: any;
  pageAbout  = '';
  isOpen = true;
  public animatePage = true;
  isGameArea:boolean=false;
  
  pageScrollClass="";

    @HostListener('window:scroll', [])

    onWindowScroll() {

      
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        this.pageScrollClass="_startScroll";
      } else {
        this.pageScrollClass="";
      }
    
    const listFloating = document.querySelectorAll('[data-floating]');
    listFloating.forEach(element => {
      const valore: number = (Number)(element.getAttribute('data-floating'));
      if (document.body.scrollTop > (element.clientTop - valore) || document.documentElement.scrollTop > (element.scrollTop - valore)) {
        element.classList.add('moveUp');
      }
    });


  }


  constructor( @Inject(DOCUMENT) private document: Document,private route: ActivatedRoute) {
    console.log('Routing app su:', this.pageActive);
    this.isOpen=false;

  }

  toggle() {
    this.isOpen = !this.isOpen;
    if(this.isOpen){
      document.getElementsByTagName('body').item(0).classList.add('toggle');
    }else{
      document.getElementsByTagName('body').item(0).classList.remove('toggle');
    }
  }

  ngOnInit() {
   
   
    this.myStyle = {
      'position': 'fixed',
      'width': '100%',
      'height': '100%',
      'z-index': -1,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
    };

    this.myParams = {
      "particles": {
        "number": {
          "value": 43,
          "density": {
            "enable": true,
            "value_area": 6155.351604731727
          }
        },
        "color": {
          "value": "#fff"
        },
        "shape": {
          "type": "circle",
          "stroke": {
            "width": 0,
            "color": "#000000"
          },
          "polygon": {
            "nb_sides": 5
          },
          "image": {
            "src": "img/github.svg",
            "width": 10,
            "height": 10
          }
        },
        "opacity": {
          "value": 0.5,
          "random": true,
          "anim": {
            "enable": false,
            "speed": 1,
            "opacity_min": 0.1,
            "sync": false
          }
        },
        "size": {
          "value": 15.782952832645451,
          "random": true,
          "anim": {
            "enable": true,
            "speed": 60.905790922600886,
            "size_min": 12.993235396821524,
            "sync": false
          }
        },
        "line_linked": {
          "enable": false,
          "distance": 500,
          "color": "#ffffff",
          "opacity": 0.4,
          "width": 1
        },
        "move": {
          "enable": true,
          "speed": 6.413648243462092,
          "direction": "top",
          "random": true,
          "straight": false,
          "out_mode": "bounce",
          "bounce": false,
          "attract": {
            "enable": true,
            "rotateX": 3126.65351868777,
            "rotateY": 1282.7296486924183
          }
        }
      },
      "interactivity": {
        "detect_on": "canvas",
        "events": {
          "onhover": {
            "enable": true,
            "mode": "bubble"
          },
          "onclick": {
            "enable": true,
            "mode": "repulse"
          },
          "resize": true
        },
        "modes": {
          "grab": {
            "distance": 400,
            "line_linked": {
              "opacity": 0.5
            }
          },
          "bubble": {
            "distance": 400,
            "size": 4,
            "duration": 0.3,
            "opacity": 1,
            "speed": 3
          },
          "repulse": {
            "distance": 200,
            "duration": 0.4
          },
          "push": {
            "particles_nb": 4
          },
          "remove": {
            "particles_nb": 2
          }
        }
      },
      "retina_detect": true
    };
  }

  mainClick(){
    if(this.isOpen){
      this.toggle();
    }
  }

  menuToggleEvent(event: any) {
      this.stateMenu = !this.stateMenu;
      this.toggle();
  }

  getStateToggle() {
      return this.pageActive + ' ' + ((this.stateMenu) ? 'toggle-active' : '');
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  showMenu(){
    this.toggle();
  }


}
