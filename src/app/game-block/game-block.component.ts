import { Component, OnInit, Input } from '@angular/core';
import Phaser from 'phaser';


class GameScene extends Phaser.Scene {

  [x: string]: any;
  green: any;
  blue: any;
  greenKeys: any;
  blueKeys: any;
  platforms:any;
  loScorreggione:any;
  cursors:any;
  gameOver:boolean;
  

  constructor() {
    super({ key: 'GameScene' });
    this.gameOver=false;
  }

  creaPiani() {

    // genera un nuovo blocco

    // creo le piattaforme di cammino
    this.platforms = this.physics.add.staticGroup();


  
    this.platforms.create(500,1000, 'ground').setScale(20).refreshBody();
    //  Now let's create some ledges
    this.platforms.create(60, 400, 'ground');
    this.platforms.create(50, 250, 'ground');
    this.platforms.create(750, 220, 'ground');

  }

  preload() {
    //this.add.bitmapText(200, 228, 'atari', 'LO SCORREGGIONE').setOrigin(0.1).setScale(0.2);  

    this.load.setBaseURL('');
    this.load.bitmapFont('atari', 'assets/font/bitmap/atari-classic.png', 'assets/font/bitmap/atari-classic.xml');
    this.load.image('blueBox', 'assets/img/blue.png');
    this.load.image('greenBox', 'assets/images/green.png');
    this.load.image('spark', 'assets/img/blue.png');
    this.load.image('ground', 'assets/img/platform.png');

    this.load.spritesheet('scorregione', 'assets/img/scorreggione.png', { frameWidth: 32, frameHeight: 48 });


  }

  createGround() {

  }


  createPlayer(){

    this.loScorreggione = this.physics.add.sprite(0,0, 'scorregione');
    this.loScorreggione.setBounce(0.2);
    this.loScorreggione.setCollideWorldBounds(true);

    this.physics.add.collider(this.loScorreggione,this.platforms);

    var hitPlatform = this.physics.collide(this.loScorreggione, this.platforms);

    //  Our player animations, turning, walking left and walking right.
    this.anims.create({
      key: 'left',
      frames: this.anims.generateFrameNumbers('scorregione', { start: 0, end: 3 }),
      frameRate: 10,
      repeat: -1
  });

  this.anims.create({
      key: 'turn',
      frames: [ { key: 'scorregione', frame: 4 } ],
      frameRate: 20
  });

  this.anims.create({
      key: 'right',
      frames: this.anims.generateFrameNumbers('scorregione', { start: 5, end: 8 }),
      frameRate: 10,
      repeat: -1
  });


  }

  create() {


    this.cursors = this.input.keyboard.createCursorKeys();

    
    this.createGround();
    this.creaPiani();

    this.createPlayer();

  }

  update() {
    if (this.gameOver)
    {
        return;
    }

    if (this.cursors.left.isDown)
    {
      this.loScorreggione.setVelocityX(-160);

      this.loScorreggione.anims.play('left', true);
    }
    else if (this.cursors.right.isDown)
    {
      this.loScorreggione .setVelocityX(160);

      this.loScorreggione.anims.play('right', true);
    }
    else
    {
      this.loScorreggione.setVelocityX(0);

      this.loScorreggione.anims.play('turn');
    }

    if (this.cursors.up.isDown &&  this.loScorreggione.body.touching.down)
    {
      this.loScorreggione.setVelocityY(-330);
    }
    
  }


}

@Component({
  selector: 'app-game-block',
  templateUrl: './game-block.component.html',
  styleUrls: ['./game-block.component.scss']
})

export class GameBlockComponent implements OnInit {
  @Input() sezione: any;

  phaserGame: Phaser.Game;
  config: Phaser.Types.Core.GameConfig;


  constructor() {

  
    this.config = {
      type: Phaser.WEBGL,
      scene:  [GameScene],
      physics: {
        default: 'arcade',
        arcade: {
          debug: true,
          gravity: {  y: 200  }
        }
      },
      scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
      },
      width: '100vw',
      height: '100vh',
      parent: 'gameblock',
      backgroundColor: '#000000'
    };

  }

  ngOnInit() {

      this.phaserGame = new Phaser.Game(this.config);

  }


}
