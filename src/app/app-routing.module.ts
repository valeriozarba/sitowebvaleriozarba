import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './page/page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { PaginaBlogComponent } from './page/pagina-blog/pagina-blog.component';
import { PaginaHomeComponent } from './page/pagina-home/pagina-home.component';
import { PaginaCurriculumComponent } from './page/pagina-curriculum/pagina-curriculum.component';
import { PaginaAboutComponent } from './page/pagina-about/pagina-about.component';
import { PaginaContattiComponent } from './page/pagina-contatti/pagina-contatti.component';
import { PaginaProgettiComponent } from './page/pagina-progetti/pagina-progetti.component';
import { PaginaSkilsComponent } from './page/pagina-skils/pagina-skils.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { GameBlockComponent } from './game-block/game-block.component';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: PaginaHomeComponent,data: {animation: 'HomePage'}},
  {path: 'curriculum', component: PaginaCurriculumComponent},
  {path: 'blog', component: PaginaBlogComponent},
  {path: 'blog/:id', component: PaginaBlogComponent},
  {path: 'about', component: PaginaAboutComponent},
  {path: 'contact', component: PaginaContattiComponent},

  {path: 'progetti', component: PaginaProgettiComponent},
  {path: 'progetti/:id', component: PaginaProgettiComponent},
  
  {path: 'game', component: GameBlockComponent},
  
  {path: 'skill', component: PaginaSkilsComponent},
  {path: 'error', component: PageComponent},
  {path: '**', redirectTo: 'error', pathMatch: 'full'}
];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled',anchorScrolling: 'enabled'}),
    BrowserAnimationsModule
  ],
  exports: [RouterModule],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class AppRoutingModule { }
