import { ViewportScroller } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-pagina-home',
  templateUrl: './pagina-home.component.html',
  styleUrls: ['./pagina-home.component.scss'],
})
export class PaginaHomeComponent implements OnInit {

  constructor(private title: Title,
    private meta: Meta,private viewportScroller: ViewportScroller) { }

  public scrollTo(elementId: string): void { 
      this.viewportScroller.scrollToAnchor(elementId);
  }

  ngOnInit() {
    this.title.setTitle('Valerio Zarba, Wordpress Developer');
    this.meta.updateTag(
      { 
        name: 'description', content: 'Se cerchi uno sviluppatore web per Wordpress sei nel sito giusto, Sviluppo siti web, e-commerce e temi per wordpress.' }
      );
  }

}
