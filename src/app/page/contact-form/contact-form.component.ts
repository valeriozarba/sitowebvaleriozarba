import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  model:any;
  submitted = false;
  form: FormGroup;
  SERVER_URL:string ="/sendmail";
  formIniziato:boolean=false;
  formCompletato:boolean=false;
  step:number=0;
  testopulsante:string="Continua";
  
  constructor(private fb: FormBuilder,private httpClient: HttpClient) { }

  goBack(){
    if(this.step>0){
      this.step-=1;
    }
  }
  onSubmit() {

    console.log(this.form);  // { first: '', last: '' }
    this.step+=1;

    if(this.step==2){
      this.testopulsante="Conferma";
    }

    if(this.step==3){
      this.formCompletato=true;
    }


    if(this.formCompletato && this.form.valid){
      this.httpClient.post<any>(this.SERVER_URL, this.form.value).subscribe(
        (res) => console.log(res),
        (err) => console.log(err)
      );
    }

  }

  ngOnInit() {
    this.form = this.fb.group({
      nome:['',Validators.required],
      cognome:[''],
      contatto:[''],
      email:['',Validators.email],
      messaggio:['']
    });
  }

  isValidForm(){
    return (this.form.valid);
  }
}
