import { Component, OnInit, Optional } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiConnectService } from 'src/app/api-connect.service';

@Component({
  selector: 'app-pagina-blog',
  templateUrl: './pagina-blog.component.html',
  styleUrls: ['./pagina-blog.component.scss']
})
export class PaginaBlogComponent implements OnInit {

  public articoli:any;
  public singolo:any;

  constructor(private servizio:ApiConnectService,
    private route: ActivatedRoute) { 

    const id = this.route.snapshot.paramMap.get('id');
    console.log("Leggo il routing id:",id);
    if(parseInt(id)>0){
      let data=this.servizio.getArticolo(parseInt(id));
      data.subscribe(object=>{
        console.log("Elemento articoli:",object);
        this.singolo=object;
      });
    }else{
      let data=servizio.getArticoli();
      data.subscribe(object=>{
        console.log("Elemento articoli:",object);
        this.articoli=object;
      });
    }
  }

  ngOnInit() {
    
  }

}
