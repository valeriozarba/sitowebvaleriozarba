import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaCurriculumComponent } from './pagina-curriculum.component';

describe('PaginaCurriculumComponent', () => {
  let component: PaginaCurriculumComponent;
  let fixture: ComponentFixture<PaginaCurriculumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaCurriculumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaCurriculumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
