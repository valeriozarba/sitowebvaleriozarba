import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaSkilsComponent } from './pagina-skils.component';

describe('PaginaSkilsComponent', () => {
  let component: PaginaSkilsComponent;
  let fixture: ComponentFixture<PaginaSkilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaSkilsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaSkilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
