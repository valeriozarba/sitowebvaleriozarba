import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ContactFormComponent } from '../contact-form/contact-form.component';
import { PaginaContattiComponent } from './pagina-contatti.component';

describe('PaginaContattiComponent', () => {
  let component: PaginaContattiComponent;
  let fixture: ComponentFixture<PaginaContattiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule,ReactiveFormsModule,BrowserModule],
      declarations: [ PaginaContattiComponent,ContactFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaContattiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
