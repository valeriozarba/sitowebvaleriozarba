import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaSlikksComponent } from './pagina-slikks.component';

describe('PaginaSlikksComponent', () => {
  let component: PaginaSlikksComponent;
  let fixture: ComponentFixture<PaginaSlikksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaSlikksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaSlikksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
