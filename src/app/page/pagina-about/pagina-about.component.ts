import { animate, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-pagina-about',
  templateUrl: './pagina-about.component.html',
  styleUrls: ['./pagina-about.component.scss'],
  animations:[
    trigger('openClose', [
      transition('open <=> *', [
        animate('5s')
      ]),
    ])
  ]
})
export class PaginaAboutComponent implements OnInit {

  constructor(private title: Title,
    private meta: Meta) { }

  ngOnInit() {
    this.title.setTitle('Chi sono | About | Valerio Zarba, Webdeveloper freelance, Libero professionista');
    this.meta.updateTag(
      { 
        name: 'description', content: 'La mia storia, il lavoro e il mio mondo fatto di sviluppo e street art.' }
      );
  }

}
