import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaAboutComponent } from './pagina-about.component';

describe('PaginaAboutComponent', () => {
  let component: PaginaAboutComponent;
  let fixture: ComponentFixture<PaginaAboutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaAboutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
