import { Component, OnInit } from '@angular/core';
import { ApiConnectService } from 'src/app/api-connect.service';
import { ActivatedRoute } from '@angular/router';
import { PostType } from 'src/app/model/postType';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-pagina-progetti',
  templateUrl: './pagina-progetti.component.html',
  styleUrls: ['./pagina-progetti.component.scss']
})
export class PaginaProgettiComponent implements OnInit {

  listaProgetti: [any];
  singolo:any;

  constructor(private sanitizer: DomSanitizer,private progetti:ApiConnectService,private route: ActivatedRoute) {

    const id = this.route.snapshot.paramMap.get('id');
    console.log("Leggo il routing id:",id);
    if(parseInt(id)>0){
      let data=this.progetti.getProgetto(parseInt(id));
      data.subscribe(object=>{
        console.log("Elemento articoli:",object);
        this.singolo=object;
      });
    }else{
      let data=progetti.getProgetti();
      data.subscribe(object=>{
        this.listaProgetti=object;
      });
    }

  }

  ngOnInit() {

   
  }

  getFeaturedImage(progetto:any){
    if(progetto.uagb_featured_image_src.medium[0] !== undefined){
      return this.sanitizer.bypassSecurityTrustResourceUrl(progetto.uagb_featured_image_src.medium[0]);
    }
    return false;
  }

}
