import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaProgettiComponent } from './pagina-progetti.component';

describe('PaginaProgettiComponent', () => {
  let component: PaginaProgettiComponent;
  let fixture: ComponentFixture<PaginaProgettiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaProgettiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaProgettiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
