import { GameBlockComponent } from './game-block/game-block.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule , CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageComponent } from './page/page.component';
import { FooterComponent } from './footer/footer.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PaginaHomeComponent } from './page/pagina-home/pagina-home.component';
import { PaginaAboutComponent } from './page/pagina-about/pagina-about.component';
import { PaginaProgettiComponent } from './page/pagina-progetti/pagina-progetti.component';
import { PaginaSlikksComponent } from './page/pagina-slikks/pagina-slikks.component';
import { PaginaSkilsComponent } from './page/pagina-skils/pagina-skils.component';
import { PaginaContattiComponent } from './page/pagina-contatti/pagina-contatti.component';
import { SocketioService } from './socketio.service';
import { PaginaCurriculumComponent } from './page/pagina-curriculum/pagina-curriculum.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { PaginaBlogComponent } from './page/pagina-blog/pagina-blog.component';
import { ContactFormComponent } from './page/contact-form/contact-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ParticlesModule } from 'angular-particle';
import { ChatComponent } from './chat/chat.component';


@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    FooterComponent,
    ToolbarComponent,
    PaginaHomeComponent,
    PaginaAboutComponent,
    PaginaProgettiComponent,
    PaginaSlikksComponent,
    PaginaSkilsComponent,
    PaginaContattiComponent,
    GameBlockComponent,
    PaginaCurriculumComponent,
    PaginaBlogComponent,
    ContactFormComponent,
    ChatComponent
  ],
  imports: [
    ParticlesModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', 
    { enabled: environment.production ,
      registrationStrategy: "registerImmediately"
    }),
  ],
  providers: [SocketioService],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }