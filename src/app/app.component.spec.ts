import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ParticlesModule } from 'angular-particle';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatComponent } from './chat/chat.component';
import { FooterComponent } from './footer/footer.component';
import { GameBlockComponent } from './game-block/game-block.component';
import { ContactFormComponent } from './page/contact-form/contact-form.component';
import { PageComponent } from './page/page.component';
import { PaginaAboutComponent } from './page/pagina-about/pagina-about.component';
import { PaginaBlogComponent } from './page/pagina-blog/pagina-blog.component';
import { PaginaContattiComponent } from './page/pagina-contatti/pagina-contatti.component';
import { PaginaCurriculumComponent } from './page/pagina-curriculum/pagina-curriculum.component';
import { PaginaHomeComponent } from './page/pagina-home/pagina-home.component';
import { PaginaProgettiComponent } from './page/pagina-progetti/pagina-progetti.component';
import { PaginaSkilsComponent } from './page/pagina-skils/pagina-skils.component';
import { PaginaSlikksComponent } from './page/pagina-slikks/pagina-slikks.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ParticlesModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ServiceWorkerModule.register('ngsw-worker.js', 
        { enabled: environment.production ,
          registrationStrategy: "registerImmediately"
        }),
      ],
      declarations: [
        AppComponent,
        PageComponent,
        FooterComponent,
        ToolbarComponent,
        PaginaHomeComponent,
        PaginaAboutComponent,
        PaginaProgettiComponent,
        PaginaSlikksComponent,
        PaginaSkilsComponent,
        PaginaContattiComponent,
        GameBlockComponent,
        PaginaCurriculumComponent,
        PaginaBlogComponent,
        ContactFormComponent,
        ChatComponent
        
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Valerio Zarba / Wordpress Developer');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    if(AppComponent.name=="PaginaHome"){
      expect(compiled.querySelector('.hero-content > h1').textContent).toContain('Cerchi uno Sviluppatore Wordpress?');
    }
  });
});
