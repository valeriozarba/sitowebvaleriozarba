import { Component, OnInit } from '@angular/core';
import { Action } from '../model/action.enum';
import { Message } from '../model/message';
import { SockEvent } from '../model/sockevent.enum';
import { User } from '../model/user';
import { SocketioService } from '../socketio.service';

@Component({
  selector: 'work-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  action = Action;
  user: User;
  messages: Message[] = [];
  messageContent: string;
  ioConnection: any;
  myChatLog:any;
  messaggio:string;
  
  constructor(private socketService: SocketioService) {
    this.user={name:"Anonymus"};
  }

  ngOnInit() {
    this.initIoConnection();
  }

  invio(){
    this.sendMessage(this.messaggio);
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    this.ioConnection = this.socketService.onMessage()
      .subscribe((message: Message) => {
        this.messages.push(message);
      });

      this.socketService.onEvent(SockEvent.CONNECT)
      .subscribe(() => {
        console.log('connected');
        this.myChatLog="connesso!";
        this.sendNotification(null,Action.JOINED);
      });
      
    this.socketService.onEvent(SockEvent.DISCONNECT)
      .subscribe(() => {
        console.log('disconnected');
        
      });
  }

  public sendMessage(message: string): void {
    if (!message) {
      return;
    }

    this.socketService.send(
      {
        from: this.user,
        content: message
      }
    );
    this.messageContent = null;

  }

  public sendNotification(params: any, action: Action): void {
    let message: Message;

    if (action === Action.JOINED) {
      message = {
        from: this.user,
        action: action
      }
    } else if (action === Action.RENAME) {
      message = {
        action: action,
        content: {
          username: this.user.name,
          previousUsername: params.previousUsername
        }
      };
    }

    this.socketService.send(message);
  }

}
