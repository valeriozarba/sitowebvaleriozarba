import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { Message } from './model/message';
import { SockEvent } from './model/sockevent.enum';

@Injectable({
    providedIn: 'root'
})
export class SocketioService {

  socket;

  constructor() { }  

  public getMessages = () => {
    return Observable.create( (observer) => {
        this.socket.on('new-message', (message) => {
            observer.next(message);
        });
    });
  }

  public initSocket(): void {
    this.socket = io(environment.SOCKET_ENDPOINT);
  }

  public send(message: Message): void {
      this.socket.emit('message', message);
  }

  public onMessage(): Observable<Message> {
      return new Observable<Message>(observer => {
          this.socket.on('message', (data: Message) => observer.next(data));
      });
  }

  public onEvent(event: SockEvent): Observable<any> {
      return new Observable<SockEvent>(observer => {
          this.socket.on(event, () => observer.next());
      });
  }

}
