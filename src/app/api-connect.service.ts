import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiConnectService {

  private ApiUrl: string = 'https://menostudio.it/wp-json/wp/v2';

  constructor(private http: HttpClient) {

  }

  getArticoli(){
    return this.http.get<any>(this.ApiUrl+"/posts")
      .pipe(
        catchError(this.handleError)
      );
  }
  getArticolo(id:number){
    return this.http.get<any>(this.ApiUrl+"/posts/"+id)
      .pipe(
        catchError(this.handleError)
      );
  }

  getProgetti(){
    return this.http.get<any>(this.ApiUrl+"/works")
      .pipe(
        catchError(this.handleError)
      );
  }

  getProgetto(id:number){
    return this.http.get<any>(this.ApiUrl+"/works/"+id)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError('Si è verificato un problema, prova a caricare la pagina!');
  }


  
}
