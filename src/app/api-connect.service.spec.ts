import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ApiConnectService } from './api-connect.service';

describe('ApiConnectService', () => {

  beforeEach(() => 
  TestBed.configureTestingModule({ 
      imports: [
        HttpClientTestingModule      
      ],
      providers: [ApiConnectService]
  }));

  it('should be created', () => {
    const service: ApiConnectService = TestBed.get(ApiConnectService);
    expect(service).toBeTruthy();
  });

  it('should have getData function', () => {
    const service: ApiConnectService = TestBed.get(ApiConnectService);
    expect(service.getArticoli).toBeTruthy();
   });

  
});
